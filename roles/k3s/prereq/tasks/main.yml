---
- name: Set SELinux to disabled state
  ansible.builtin.selinux:
    state: disabled
  when: ansible_distribution in ['CentOS', 'Red Hat Enterprise Linux']

- name: Enable IPv4 forwarding
  ansible.builtin.sysctl:
    name: net.ipv4.ip_forward
    value: "1"
    state: present
    reload: yes

- name: Enable IPv6 forwarding
  ansible.builtin.sysctl:
    name: net.ipv6.conf.all.forwarding
    value: "1"
    state: present
    reload: yes
  when: ansible_all_ipv6_addresses

- name: Add br_netfilter to /etc/modules-load.d/
  ansible.builtin.copy:
    content: "br_netfilter"
    dest: /etc/modules-load.d/br_netfilter.conf
    mode: "u=rw,g=,o="
  when: ansible_distribution in ['CentOS', 'Red Hat Enterprise Linux']

- name: Load br_netfilter
  ansible.builtin.modprobe:
    name: br_netfilter
    state: present
  when: ansible_distribution in ['CentOS', 'Red Hat Enterprise Linux']

- name: Set bridge-nf-call-iptables (just to be sure)
  ansible.builtin.sysctl:
    name: "{{ item }}"
    value: "1"
    state: present
    reload: yes
  when: ansible_distribution in ['CentOS', 'Red Hat Enterprise Linux']
  loop:
    - net.bridge.bridge-nf-call-iptables
    - net.bridge.bridge-nf-call-ip6tables

- name: Add /usr/local/bin to sudo secure_path
  ansible.builtin.lineinfile:
    line: 'Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin'
    regexp: "Defaults(\\s)*secure_path(\\s)*="
    state: present
    insertafter: EOF
    path: /etc/sudoers
    validate: 'visudo -cf %s'
  when: ansible_distribution in ['CentOS', 'Red Hat Enterprise Linux']

- name: Disable swap.
  ansible.builtin.shell: |
    swapoff -a
  when: ansible_distribution in ['Ubuntu']

- name: Permanently disable swap.
  ansible.builtin.replace:
    path: /etc/fstab
    regexp: '^([^#].*?\sswap\s+sw\s+.*)$'
    replace: '# \1'
  when: ansible_distribution in ['Ubuntu']

- name: Install longhorn dependencies.
  ansible.builtin.apt:
    name:
      - jq
      - nfs-common
      - open-iscsi
  when: ansible_distribution in ['Ubuntu','Debian']
