all:
  children:
    pi0:
      hosts: 192.168.192.1 # Either eth0 or eth1 ip address, depending network you're connected to.
    cluster_node:
      children:
        k3s_init_server_node:
          hosts:
            192.168.192.101:
        k3s_server_node:
          hosts:
            192.168.192.102:
            192.168.192.103:
        k3s_agent_node:
          hosts:
            192.168.192.120:
    workstation:
      hosts:
        localhost
  vars:
    # =========================== Pi0 install =======================================
    # Ansible
    ansible_user: pi
    ansible_python_interpreter: /usr/bin/python
    # Pi0 network devices.
    pi0_local_network_ethernet_device: eth1
    pi0_cluster_network_ethernet_device: eth0
    # Local network.
    local_network_gateway_ip: 192.168.10.1
    local_network_pi0_ip: 192.168.10.100
    # Cluster network.
    dhcp_hosts:
      - name: pi0
        mac: 'e4:5f:01:2b:77:d4'
        ip: 192.168.192.1
      - name: pi1
        mac: 'e4:5f:01:2b:78:19'
        ip: 192.168.192.101
      - name: pi2
        mac: 'e4:5f:01:2b:77:f2'
        ip: 192.168.192.102
      - name: pi3
        mac: 'e4:5f:01:2b:77:f5'
        ip: 192.168.192.103
      - name: blue1
        mac: '00:e0:4c:08:de:fa'
        ip: 192.168.192.120
    dhcp_subnets:
      - interface: "{{ pi0_cluster_network_ethernet_device }}"
        ip: 192.168.192.0
        netmask: 255.255.255.0
        domain_name_servers:
          - "{{ dhcp_hosts[0].ip }}"
          - 8.8.8.8
        range_begin: 192.168.192.100
        range_end: 192.168.192.255
        routers: "{{ dhcp_hosts[0].ip }}"
    # Port definitions. Node ports range: 30000 - 32767.
    pi0_http_proxy_port: 32080 # Pi0 incoming http traffic from local network.
    cluster_http_node_port: 32080 # Cluster incoming http traffic.
    pi0_https_proxy_port: 32443 # Pi0 incoming https traffic from local network.
    cluster_https_node_port: 32443 # Cluster incoming https traffic.
    pi0_gitea_ssh_proxy_port: 32022 # Pi0 incoming gitea ssh traffic.
    cluster_gitea_ssh_node_port: 32022 # Cluster incoming gitea ssh traffic.
    # Pivpn.
    pivpn_port: 49993
    pivpn_protocol: udp4
    pivpn_host: vpn.your.domain
    # Dynamic DNS.
    ddclient_conf:
      - name: Dynu
        protocol: dyndns2
        use: web, web=checkip.dynu.com/, web-skip='IP Address'
        server: api.dynu.com
        login: username
        password: 'very-secure-secret'
        address: your.domain
    # Minio object store.
    minio_server_ip:  "{{ dhcp_hosts[0].ip }}"
    minio_server_port: 9091
    minio_root_user: admin
    minio_root_password: 'very-secure-secret'

    # =========================== Cluster install =======================================
    # K3s.
    k3s_version: v1.21.9+k3s1 # Check https://update.k3s.io/v1-release/channels.
    k3s_token: 'very-secure-secret' # Set a unique token.

    # =========================== Cluster software install ==============================
    # Longhorn persisten storage.
    longhorn_ui_node_port: 31418 # Lonhorn ui cluster node port, range: 30000 - 32767.
    longhorn_backup_minio_user: longhorn
    longhorn_backup_minio_password: 'very-secure-secret'
    longhorn_backup_minio_bucket_name: longhorn-backup
    # Gitea.
    gitea_domain: git.your.domain
    gitea_database_password: 'very-secure-secret'
    gitea_username: 'username'
    gitea_email: 'username@your.domain'
    gitea_password: 'very-secure-secret'
    gitea_image: gitea/gitea:1.15.10
    # Cluster pull through Docker registry
    cluster_registry_http_node_port: 32555 # Port to access the registry within the cluster.
    registry_domain: registry.your.domain
    registry_password: 'very-secure-secret'
    # Letsencrypt
    letsencrypt_email: 'user@your.domain'

    # ================= no need to change ====================================
    # Pi0.
    # PiVpn role.
    pivpn_dev: "{{ pi0_local_network_ethernet_device }}"
    pivpn_addr: "{{ local_network_pi0_ip }}" # Cluster router eth1 ip
    pivpn_gw: "{{ local_network_gateway_ip }}" # Local network gateway ip
    pivpn_install_user: pi
    pivpn_dns_1: "{{ local_network_gateway_ip }}" # local network gateway ip
    pivpn_dns_2: 8.8.8.8 # Dns server 2
    # Bertvv.dhcp role.
    dhcp_global_authoritative: authoritative
    # routing role.
    cluster_router_eth0_ip: "{{ dhcp_hosts[0].ip }}"
    cluster_router_eth1_ip: "{{ local_network_pi0_ip }}"
    lan_router_ip: "{{ local_network_gateway_ip }}"
    # atosatto.minio.
    minio_server_addr: ':{{ minio_server_port }}'
    minio_server_env_extra: |
      MINIO_ROOT_USER={{ minio_root_user }}
      MINIO_ROOT_PASSWORD='{{ minio_root_password }}'
    # Cluster nodes.
    # K3s role.
    systemd_dir: /etc/systemd/system
    k3s_init_server_node_ip: "{{ hostvars[groups['k3s_init_server_node'][0]]['ansible_host'] | default(groups['k3s_init_server_node'][0]) }}"
    extra_server_args: ""
    extra_agent_args: ""
    # Kubernetes.
    # Cluster software role.
    kube_context: default
    kubeconfig_path: "{{ ansible_env.HOME }}/.kube/config"
    # Ingress nginx
    content_security_policy: default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self'; font-src 'self'; base-uri 'self'; form-action 'self';
