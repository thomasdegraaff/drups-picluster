Ansible playbook to install the needed software on the cluster nodes.

Instructions on how to create your own Drups devops cluster in about three hours can be found on the [drups.eu](https://drups.eu) website.
